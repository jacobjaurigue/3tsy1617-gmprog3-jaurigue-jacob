﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Movement : MonoBehaviour {

	NavMeshAgent koko;
	Animator animator;
	public GameObject Marker;

	// Use this for initialization
	void Start () {
		koko = GetComponent<NavMeshAgent>();
		animator = GetComponent<Animator> ();
		animator.SetInteger ("AnimIndex", 0);
	}
	
	// Update is called once per frame
	void Update () {
		if (koko.remainingDistance < 1) {
			animator.SetInteger ("AnimIndex", 0);
		}
		Move ();
	}

	void Move() 
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			Ray RayCast = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit Hit;

			if (Physics.Raycast (RayCast, out Hit, Mathf.Infinity)) {
				{
					koko.destination = Hit.point;
					animator.SetInteger ("AnimIndex", 1);
					Marker.SetActive (true);
					Marker.transform.position = Hit.point;
				}
			}
		}
	}
}
