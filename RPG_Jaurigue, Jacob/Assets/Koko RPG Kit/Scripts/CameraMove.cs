﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {

    public GameObject Koko;
    private float SmoothingSpeed = 4.0f;
    private Vector3 offset;
    // Use this for initialization
    void Start()
    {
        offset = transform.position - Koko.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float PanSpeed = (SmoothingSpeed * Time.deltaTime);
        transform.position = Vector3.Lerp(transform.position, Koko.transform.position + offset, PanSpeed);
    }
}
