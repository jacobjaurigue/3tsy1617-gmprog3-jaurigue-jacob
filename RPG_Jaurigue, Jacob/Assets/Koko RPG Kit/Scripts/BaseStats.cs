﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseStats : MonoBehaviour
{
    public Slider HpBar;
    public int maxHp;
    public int currentHealth;
    public int inteligence;
    public int spirit;
    public int vitality;
    public int strength;
    public int manapoints;
    public int Hp
    {
        get { return vitality * 10; }
        set { vitality = value; }
    }
    public int MpRegen
    {
        get { return spirit / 10; }
        set { spirit = value; }
    }

    public int MAttack
    {
        get { return inteligence * 2; }
        set { inteligence = value; }
    }
    public int Attack
    {
        get { return strength * 2; }
        set { strength = value; }
    }

    // Use this for initialization
    void Start()
    {
        currentHealth = Hp;
        maxHp = Hp;
        HpBar.maxValue = maxHp;
        HpBar.value = currentHealth;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        HpBar.value = currentHealth;
    }
}
