﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

    public GameObject koko, 
                      monster, 
                      npc;

    public void kokoCam()
    {
        Camera.main.transform.position = new Vector3(8, 3, 10);
        koko.SetActive(true);
        monster.SetActive(false);
        npc.SetActive(false);
    }

    public void monsterCam()
    {
        Camera.main.transform.position = new Vector3(-13, 3, 5);
        koko.SetActive(false);
        monster.SetActive(true);
        npc.SetActive(false);
    }

    public void npcCam()
    {
        Camera.main.transform.position = new Vector3(-37, 3, 7);
        koko.SetActive(false);
        monster.SetActive(false);
        npc.SetActive(true);
    }

}
