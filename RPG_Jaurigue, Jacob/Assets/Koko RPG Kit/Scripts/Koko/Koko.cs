﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Koko : StateMachine
{
    public enum Actions { Idle, Run, Chase, Attack, Skill, Dead }
    NavMeshAgent koko;
    public float exp, nextLvl, currentHp, currentGold, currentMp;
    [HideInInspector]
    public KokoStats kokoStats;
    public GameObject inventory;
    public Slider health;
    public Slider mana;
    public Text GoldText;
    public GameObject statsUI;
    BaseStats stats;
    public Animator anim;
    public LayerMask Ground;
    public LayerMask Monster;
    public GameObject Pointer;
    GameObject pointer;
    RaycastHit hit;
    Ray ray;
    private const float rotSpeed = 20f;

    // Use this for initialization
    void Start()
    {
        koko = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        anim.SetTrigger ("Idle");
        prevState = Fsm_State.Idle;
        curState = Fsm_State.Idle;


        exp = 0;
        nextLvl = 150;
        currentGold = 0;

        currentHp = 50;
        health.maxValue = 100;
        health.value = currentHp;

        currentMp = 10;
        mana.maxValue = 50;
        mana.value = currentMp;
    }

    public void AddHp()
    {
        currentHp += 50;
        health.value = currentHp;
    }

    public void AddMp()
    {
        currentMp += 15;
        mana.value = currentMp;
    }

    public void AddBuff()
    {

    }

    public void AddGold()
    {
        currentGold += 10;
    }

    // Update is called once per frame
    void Update()
    {
        FSMUpdate();
        InstantlyTurn(koko.destination);
        if (koko.remainingDistance < 1)
        {
            anim.SetTrigger("Idle");
        }

        if (exp >= nextLvl)
        {
            //kokoStats.levelUp();
            exp = 0;
            nextLvl += nextLvl * 1;
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            inventory.SetActive(true);

        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            inventory.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            Destroy(GameObject.FindWithTag("Enemy"));
            
        }

        /*if (Input.GetKeyDown(KeyCode.S))
        {
            stats.SetActive(true);
        }*/
 
        GoldText.text = "Gold: " + currentGold.ToString();
    }

    private void InstantlyTurn(Vector3 destination)
    {
        //When on target -> dont rotate!
        if ((destination - transform.position).magnitude < 0.1f) return;

        Vector3 direction = (destination - transform.position).normalized;
        Quaternion qDir = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, qDir, Time.deltaTime * rotSpeed);
    }

    void DamageTo()
    {
        if (hit.collider != null)
        {
            hit.transform.gameObject.GetComponent<BaseStats>().TakeDamage(stats.Attack);
        }
    }

    protected override void FSMUpdate()
    {
        switch (curState)
        {
            case Fsm_State.Patrol:
                UpdatePatrol();
                break;
            case Fsm_State.Chase:
                UpdateChase();
                break;
            case Fsm_State.Attack:
                UpdateAttack();
                break;
            case Fsm_State.Dead:
                UpdateDead();
                break;
            default:
                UpdateIdle();
                break;
        }
    }

    protected void UpdateIdle()
    {
        if (prevState != curState)
        {
            prevState = curState;
        }
        Click();
    }
    protected void UpdatePatrol()
    {
        if (prevState != curState)
        {
            prevState = curState;
        }
        koko.SetDestination(hit.point);
        Click();

    }
    protected void UpdateChase()
    {
        if (prevState != curState)
        {
            prevState = curState;
        }
        koko.SetDestination(hit.point);
        if (Vector3.Distance(this.transform.position, hit.point) < 3)
        {
            anim.SetTrigger("Attack");
            curState = Fsm_State.Attack;
        }
        
        Click();
    }

    protected void UpdateAttack()
    {
        if (prevState != curState)
        {
            prevState = curState;
        }
        Click();
    }

    protected void UpdateDead()
    {
        if (prevState != curState)
        {
            prevState = curState;
        }
    }

    void Click()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Input.GetMouseButton(0))
            {
                anim.SetTrigger("Run");
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, Monster))
                {
                    if (pointer != null)
                    {
                        Destroy(pointer);
                    }
                    anim.SetTrigger("Attack Run");
                    curState = Fsm_State.Chase;
                }
                else if (Physics.Raycast(ray, out hit, Mathf.Infinity, Ground))
                {
                    if (pointer != null)
                    {
                        Destroy(pointer);
                    }
                    pointer = (GameObject)
                    (Instantiate(Pointer, hit.point, Quaternion.identity));
                    curState = Fsm_State.Patrol;
                }
            }
        }
    }

}