﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillExp : MonoBehaviour {

    public Koko koko;
    public Image fillXP;
    // Use this for initialization

    void Update ()
    {
        fillXP.fillAmount = koko.exp / koko.nextLvl;
        if (fillXP.fillAmount == 1)
        {
            fillXP.fillAmount = 0;
        }
	}
	
}
