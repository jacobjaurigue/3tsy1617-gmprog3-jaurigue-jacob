﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadAnimation : MonoBehaviour {

    Animator animator;

    public void Button(string animate)
    {
        animator = GetComponent<Animator>();
        animator.SetTrigger(animate);
    }
}
