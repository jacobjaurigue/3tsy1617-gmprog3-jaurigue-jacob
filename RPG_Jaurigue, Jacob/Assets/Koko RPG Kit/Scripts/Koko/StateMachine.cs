﻿using UnityEngine;
using System.Collections;

public class StateMachine : MonoBehaviour
{

    protected virtual void Initialize() { }
    protected virtual void FSMUpdate() { }

    public enum Fsm_State   {None, Idle, Patrol, Chase, Attack, Run, Dead, Skill}
    public Fsm_State curState;
    public Fsm_State prevState;
}
