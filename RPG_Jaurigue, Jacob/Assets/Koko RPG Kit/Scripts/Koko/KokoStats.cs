﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class KokoStats : BaseStats
{

    public Slider expSlider;
    public int exp;
    public int level;
    public int nxtLevel;
    public int skillPoints;
    int pastattack;
    float AttackCooldown = 10f;
    public float AttackUpLeft = 0f;
    void Start()
    {
        setStats();
    }
    void setStats()
    {
        exp = 0;
        level = 1;
        nxtLevel = 400;
        expSlider.value = exp;
        expSlider.maxValue = nxtLevel;
        currentHealth = Hp;
        maxHp = currentHealth;
        HpBar.maxValue = maxHp;
        HpBar.value = currentHealth;

    }
    void Update()
    {
        if (exp >= nxtLevel)
        {
            LevelUp();
        }
    }
    public void addExp(int x)
    {
        exp += x;
        expSlider.value = exp;
    }

    public void LevelUp()
    {
        level++;
        nxtLevel = nxtLevel * 3;
        expSlider.maxValue = nxtLevel;
        expSlider.value = 0;
        skillPoints += 10;
        exp = 0;
        currentHealth = Hp;
        HpBar.value = currentHealth;
    }

    public void AddVitality(int i)
    {
        if (skillPoints > 0)
        {
            vitality += i;
            maxHp = Hp;
            HpBar.maxValue = maxHp;
            skillPoints--;
        }
    }

    public void AddStrength(int i)
    {
        if (skillPoints > 0)
        {
            strength += i;
            skillPoints--;
        }
    }
    public void Heal(int i)
    {

        currentHealth += i;
        if (currentHealth > maxHp)
        {
            currentHealth = maxHp;
        }
        HpBar.value = currentHealth;
    }
    public void AttackUp()
    {
        pastattack = strength;
        strength = pastattack * 5;
        AttackUpLeft = AttackCooldown;

    }
    public void ManaUp(int x)
    {
        manapoints += x;
    }


}
