﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Movement : MonoBehaviour
{
    public GameObject Pointer;
    GameObject pointer;
    Vector3 offset;
    public LayerMask Ground;
    RaycastHit hit;
    NavMeshAgent agent;
    Animator anim;

    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        offset = new Vector3(0, 0.5f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        Click();
    }

    void Click()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButton(0))
        {
            //Get mouse hit position
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, Ground))
            {
                //Check if pointer exists, if so then destroy
                if (pointer != null)
                {
                    Destroy(pointer);
                }
                //If not, instantiate pointer at mouse hit position
                pointer = (GameObject)
                    (Instantiate(Pointer, hit.point + offset, Quaternion.identity));
                //Set player to move towards pointer
                Move(hit.point);
            }
        }
    }
    void Move(Vector3 place)
    {
        anim.SetTrigger("Run");
        agent.SetDestination(place);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "pointer")
        {
            anim.SetTrigger("Idle");
            Destroy(other.gameObject);
        }
    }

}
