﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatePattern : MonoBehaviour
{

    public float searchSpeed = 120f;
    public float searchDuration = 4f;
    public float searchRange = 20f;
    public Animator animator;
    public Transform eyes;
    [HideInInspector]
    public Vector3 offset = new Vector3 (0, 0, 0);
    public GameObject[] wayPoints;
    public IEnemyState currentState;
    [HideInInspector]
    public Transform targetChase;
    public ChaseState chaseState;
    public PatrolState patrolState;
    public AttackState attackState;
    public IdleState alertState;
    [HideInInspector]
    public UnityEngine.AI.NavMeshAgent navMeshAgent;
    BaseStats stats;
    public GameObject[] items;
    public int randItem;
    public GameObject Gold;
    private const float rotSpeed = 20f;

    public enum states { Idle, Walk, Run, Attack, Dead }
    public states monsterStates;

    private void Awake()
    {
        chaseState = new ChaseState(this);
        patrolState = new PatrolState(this);
        attackState = new AttackState(this);
        alertState = new IdleState(this);
        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    // Use this for initialization
    void Start()
    {
        currentState = patrolState;
    }

    // Update is called once per frame
    void Update()
    {
        changeState();
        currentState.UpdateState();
    }

    public void changeState()
    {
        switch (monsterStates)
        {
            case states.Idle:
                animator.SetTrigger("Idle");
                break;
            case states.Walk:
                animator.SetTrigger("Walk");
                break;
            case states.Run:
                animator.SetTrigger("Run");
                break;
            case states.Attack:
                animator.SetTrigger("Attack");
                break;
            case states.Dead:
                animator.SetTrigger("Dead");
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        currentState.OnTriggerEnter(other);
    }
}
