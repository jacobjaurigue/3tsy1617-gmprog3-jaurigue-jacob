﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : BaseStats
{

    // Use this for initialization
    void Start()
    {
        currentHealth = Hp;
        maxHp = Hp;
        HpBar.maxValue = maxHp;
        HpBar.value = currentHealth;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
