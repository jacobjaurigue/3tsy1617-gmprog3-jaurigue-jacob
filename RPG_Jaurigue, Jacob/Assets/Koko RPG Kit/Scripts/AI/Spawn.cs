﻿﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour
{
    public GameObject[] SpawnPoint;
    public GameObject[] Enemy;
    public float SpawnRate;
    private float SpawnNew;

    // Use this for initialization
    void Start()
    {
        SpawnEnemy();
        InvokeRepeating("SpawnEnemy", SpawnRate, 10);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpawnEnemy()
    {
        int randSpawn = Random.Range(0, SpawnPoint.Length);
        int randEnemy = Random.Range(0, Enemy.Length);
        if (Time.time > SpawnNew)
        {
            SpawnNew = Time.time * SpawnRate;
            Instantiate(Enemy[randEnemy], SpawnPoint[randSpawn].transform.position, SpawnPoint[randSpawn].transform.rotation);
        }
    }
}

