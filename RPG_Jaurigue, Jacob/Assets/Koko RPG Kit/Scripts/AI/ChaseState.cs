﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseState : IEnemyState
{

    private readonly StatePattern monster;
    private float targetDistance;

    public ChaseState(StatePattern statepat)
    {
        monster = statepat;
    }

    public void UpdateState()
    {
        targetDistance = Vector3.Distance(monster.transform.position, monster.targetChase.position);
        Look();
        Chase();
    }

    public void OnTriggerEnter(Collider other)
    {
    }

    public void ToPatrolState()
    {
    }

    public void ToAlertState()
    {
        monster.currentState = monster.alertState;
    }

    public void ToChaseState()
    {
    }

    public void ToAttackState()
    {
        monster.currentState = monster.attackState;
    }

    private void Look()
    {
        RaycastHit Hit;
        Vector3 monsterRemainingDistance = (monster.targetChase.position + monster.offset) - monster.eyes.position;
        if (Physics.Raycast(monster.eyes.position, monsterRemainingDistance, out Hit, monster.searchRange)
            && Hit.collider.CompareTag("Koko"))
        {
            monster.targetChase = Hit.transform;
        }
        else if (targetDistance < 3)
        {
            ToAttackState();
        }
        else if (targetDistance > 3.5)
        {
            ToAlertState();
        }
    }

    private void Chase()
    {
        if (targetDistance > 1)
        {
            monster.monsterStates = StatePattern.states.Run;
            monster.navMeshAgent.destination = monster.targetChase.position;
            monster.navMeshAgent.Resume();
        }
        else if (targetDistance < 2)
        {
            ToAttackState();
        }
    }


}

