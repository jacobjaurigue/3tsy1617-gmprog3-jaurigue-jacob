﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IEnemyState
{

    private readonly StatePattern monster;
    private float searchDuration;

    public IdleState(StatePattern statepat)
    {
        monster = statepat;
    }

    public void UpdateState()
    {
        Look();
        Search();

        if (searchDuration >= monster.searchDuration)
        {
            ToPatrolState();
        }
    }

    public void ToPatrolState()
    {
        monster.currentState = monster.patrolState;
    }

    public void ToAlertState()
    {
    }

    public void ToChaseState()
    {
        monster.currentState = monster.chaseState;
    }

    public void ToAttackState()
    {
    }

    public void OnTriggerEnter(Collider other)
    {
    }

    private void Look()
    {
        RaycastHit Hit;
        if (Physics.Raycast(monster.eyes.position, monster.eyes.forward, out Hit,
            monster.searchRange) && Hit.collider.CompareTag("Koko"))
        {
            monster.targetChase = Hit.transform;
            ToAttackState();
        }
    }

    private void Search()
    {
        monster.monsterStates = StatePattern.states.Idle;
        monster.navMeshAgent.Stop();
    }
}
