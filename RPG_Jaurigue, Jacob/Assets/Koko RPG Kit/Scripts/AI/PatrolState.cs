﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : IEnemyState
{

    private readonly StatePattern monster;
    private int nextwayPoint;

    public PatrolState(StatePattern statepat)
    {
        monster = statepat;
    }

    public void UpdateState()
    {
        Look();
        Patrol();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Koko"))
        {
            ToAlertState();
        }
    }

    public void ToPatrolState()
    {
    }

    public void ToAlertState()
    {
        monster.currentState = monster.chaseState;
    }

    public void ToChaseState()
    {
        monster.currentState = monster.chaseState;
    }

    public void ToAttackState()
    {

    }

    private void Look()
    {
        RaycastHit Hit;
        if (Physics.Raycast(monster.eyes.position, monster.eyes.forward, out Hit) && Hit.collider.CompareTag("Koko"))
        { 
                monster.targetChase = Hit.transform;
                ToChaseState();
        }
    }

    private void Patrol()
    {
        monster.monsterStates = StatePattern.states.Walk;
        monster.navMeshAgent.destination = monster.wayPoints[nextwayPoint].transform.position;
        monster.navMeshAgent.Resume();
        if (monster.navMeshAgent.remainingDistance <= monster.navMeshAgent.stoppingDistance &&
           !monster.navMeshAgent.pathPending)
        {
            nextwayPoint = (nextwayPoint + 1) % monster.wayPoints.Length;
        }
    }

}
