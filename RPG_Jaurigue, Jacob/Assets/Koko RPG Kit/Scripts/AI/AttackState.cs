﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : IEnemyState
{

    private readonly StatePattern monster;
    private float targetDistance;
    BaseStats stats;

    public AttackState(StatePattern statepat)
    {
        monster = statepat;
    }

    public void UpdateState()
    {
        targetDistance = Vector3.Distance(monster.transform.position, monster.targetChase.position);
        Attack();
    }

    public void OnTriggerEnter(Collider other)
    {
    }

    public void ToPatrolState()
    {
    }

    public void ToAlertState()
    {
        monster.currentState = monster.alertState;
    }

    public void ToChaseState()
    {
        monster.currentState = monster.chaseState;
    }

    public void ToAttackState()
    {
    }

    public void Attack()
    {
        if (targetDistance < 1)
        {
            monster.monsterStates = StatePattern.states.Attack;
        }
        else if (targetDistance > 1)
        {
            ToChaseState();
        }
        else if (targetDistance > 3.5)
        {
            ToAlertState();
        }
    }

    void DamageTo()
    {
        if (monster.targetChase != null)
        {
            monster.targetChase.GetComponent<BaseStats>().TakeDamage(stats.Attack);
        }
    }
}
