﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyState
{

    void UpdateState();

    void ToPatrolState();

    void ToAlertState();

    void ToChaseState();

    void ToAttackState();

    void OnTriggerEnter(Collider other);
}
