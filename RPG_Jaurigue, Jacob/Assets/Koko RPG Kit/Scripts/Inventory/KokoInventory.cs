﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KokoInventory : MonoBehaviour {

    public GameObject hpImage;
    public GameObject hpButton;
    public GameObject mpImage;
    public GameObject mpButton;
    public GameObject bfImage;
    public GameObject bfButton;

    void Update()
    {

    }

    public void Potion(Button hpButton)
    {
        hpImage.SetActive(false);
        hpButton.GetComponent<Button>().interactable = false;
        GameObject.FindGameObjectWithTag("Koko").GetComponent<Koko>().AddHp();
    }

    public void ManaPotion(Button mpButton)
    {
        mpImage.SetActive(false);
        mpButton.GetComponent<Button>().interactable = false;
        GameObject.FindGameObjectWithTag("Koko").GetComponent<Koko>().AddMp();
    }
    public void BuffPotion(Button bfButton)
    {
        bfImage.SetActive(false);
        bfButton.GetComponent<Button>().interactable = false;
        GameObject.FindGameObjectWithTag("Koko").GetComponent<Koko>().AddBuff();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Potion")
        {
            hpImage.SetActive(true);
            hpButton.GetComponent<Button>().interactable = true;
        }
        else if (other.gameObject.tag == "ManaPotion")
        {
            mpImage.SetActive(true);
            mpButton.GetComponent<Button>().interactable = true;
        }
        else if (other.gameObject.tag == "BuffPotion")
        {
            bfImage.SetActive(true);
            bfButton.GetComponent<Button>().interactable = true;
        }
        else if (other.gameObject.tag == "Gold")
        {
            GameObject.FindGameObjectWithTag("Koko").GetComponent<Koko>().AddGold();
        }
    }
}
