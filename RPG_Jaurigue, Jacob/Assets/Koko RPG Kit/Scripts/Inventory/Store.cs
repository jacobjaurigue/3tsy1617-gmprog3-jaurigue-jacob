﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Store : MonoBehaviour {

    public GameObject store;
    public GameObject inventory;
    public GameObject hpButton;
    public GameObject hpImage;
    public GameObject mpButton;
    public GameObject mpImage;
    public GameObject bfButton;
    public GameObject bfImage;
    public GameObject storeHP;
    public GameObject storeMP;
    public GameObject storeBF;
    public bool isNear;

    void Update()
    {
            if (isNear == true && Input.GetKeyDown(KeyCode.P))
            {
                store.SetActive(true);
                inventory.SetActive(true);
                hpButton.GetComponent<Button>().interactable = true;
                mpButton.GetComponent<Button>().interactable = true;
                bfButton.GetComponent<Button>().interactable = true;
            }
    }

    public void Potion(Button storeHP)
    {
        hpImage.SetActive(true);
        hpButton.GetComponent<Button>().interactable = true;
    }

    public void ManaPotion(Button storeMP)
    {
        mpImage.SetActive(true);
        mpButton.GetComponent<Button>().interactable = true;
    }
    public void BuffPotion(Button storeBF)
    {
        bfImage.SetActive(true);
        bfButton.GetComponent<Button>().interactable = true;
    }

    void OnTriggerStay(Collider other)
    {
        Debug.Log ("true");
        if (other.gameObject.tag == "Koko")
        {
            isNear = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Koko")
        {
            isNear = false;
            store.SetActive(false);
            inventory.SetActive(false);
        }
    }
}
