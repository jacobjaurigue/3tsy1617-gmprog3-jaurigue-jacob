﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnItem : MonoBehaviour {

    public GameObject[] items;
    int spawnNum = 1;

	// Use this for initialization
	void Start () {
        spawn();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void spawn()
    {
        int randItem = Random.Range(0, items.Length);

        

        for (int i = 0; i < spawnNum; i++)
        {
            Vector3 itemPos = new Vector3(GameObject.FindWithTag("Enemy").transform.position.x + 2,
                                          GameObject.FindWithTag("Enemy").transform.position.y + 2, 
                                          GameObject.FindWithTag("Enemy").transform.position.z + 2);

            Instantiate(items[randItem], itemPos, Quaternion.identity);
        }
    }
}
